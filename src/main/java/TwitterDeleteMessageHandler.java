import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


public class TwitterDeleteMessageHandler {

    private TwitterStream twitterStream;
    private ConfigurationBuilder cb;
    private StatusListener listener;
    private AtomicReference<Integer> deleteMessagesCount;
    private Socket logstashSocket;

    /**
     * Constructor for TwitterDeleteMessageHandler
     */
    public TwitterDeleteMessageHandler() {
        cb = new ConfigurationBuilder();
        deleteMessagesCount = new AtomicReference<>(0);
    }

    /**
     * TwitterStream Initialization and attaching listener with implemented methods
     * @throws IOException
     */
    void openTwitterStream() throws IOException {
        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        attachListener();
        logstashSocket = new Socket("localhost", 5400);

    }

    /**
     * Listen twitter stream, collect delete messages per minutes and send it to Logstash
     */
    void listen(){
        Runnable sendToLogstash = () -> {
            PrintWriter logstashPrintWriter;
            try {
                logstashPrintWriter = new PrintWriter(logstashSocket.getOutputStream(), true);
                logstashPrintWriter.println("{\"deleteMessageCount\": " + deleteMessagesCount.get() + " }");
            } catch (IOException e) {
                e.printStackTrace();
            }
            deleteMessagesCount.set(0);
        };

        twitterStream.sample();

        ScheduledExecutorService service = Executors
                .newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(sendToLogstash, 0, 60, TimeUnit.SECONDS);

    }


    /**
     * Attaching listener with implmeneted methods to twitter stream
     */
    private void attachListener(){
        listener = new StatusListener() {

            public void onStatus(Status status) {
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                deleteMessagesCount.set(deleteMessagesCount.get() + 1);
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            public void onScrubGeo(long userId, long upToStatusId) {
            }

            public void onStallWarning(StallWarning warning) {
            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);
    }
}
