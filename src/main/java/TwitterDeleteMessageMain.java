import java.io.IOException;

public class TwitterDeleteMessageMain {

    /**
     * Main application class
     * @param args
     */
    public static void main(String[] args){
        TwitterDeleteMessageHandler twitterDeleteMessageHandler = new TwitterDeleteMessageHandler();
        try {
            twitterDeleteMessageHandler.openTwitterStream();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        twitterDeleteMessageHandler.listen();
    }

}
